#!/bin/bash

# Assign mounted docker socket to docker group and give it rwx permissions.
chgrp docker /var/run/docker.sock
chmod g=rwx /var/run/docker.sock

# Switch into dev user
cd /home/dev/projects
su dev -s /bin/bash