.PHONY: lint build test snyk-auth snyk-test grype dev

lint:
	hadolint Dockerfile

build:
	docker build -t test-agent .

test:
	container-structure-test test --image test-agent --config test/config.yaml

snyk-auth:
	snyk auth ${SNYK_TOKEN}

snyk-test:
	snyk container test test-agent --fail-on=patchable --file=Dockerfile --exclude-base-image-vulns --json | snyk-to-html -o reports/snyk_results.html

grype:
	grype docker:test-agent

dev: lint build test snyk-auth snyk-test grype
