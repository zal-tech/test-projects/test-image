After running the container, exec into with `docker exec -it -u root dev-agent ./start-dev.sh`

Note: The build pipeline uses docker save/load rather than push/pull to avoid needing registry write permissions during development.
