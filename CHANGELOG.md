# [1.2.0](https://gitlab.com/zal-tech/test-projects/test-image/compare/v1.1.0...v1.2.0) (2021-07-06)


### New

* Added analysis tools Syft and Grype. ([efbc471](https://gitlab.com/zal-tech/test-projects/test-image/commit/efbc471392fcf1a3e0217995c8900dba74de8d0c)), closes [#2](https://gitlab.com/zal-tech/test-projects/test-image/issues/2)

# [1.1.0](https://gitlab.com/zal-tech/test-projects/test-image/compare/v1.0.3...v1.1.0) (2021-07-03)


### Update

* Switched to using docker save/load ([3fbe244](https://gitlab.com/zal-tech/test-projects/test-image/commit/3fbe244e26abcdb1a206f13a6bff8a257beb7548))

## [1.0.3](https://gitlab.com/zal-tech/test-projects/test-image/compare/v1.0.2...v1.0.3) (2021-06-14)


### Fix

* Testing adding docker to snyk image. ([c4e9bff](https://gitlab.com/zal-tech/test-projects/test-image/commit/c4e9bfff1ae6d9eed3af104968942246e64814d8))
* Testing adding docker to snyk image. ([a8d6190](https://gitlab.com/zal-tech/test-projects/test-image/commit/a8d6190f934be4332ef58a77d0a2f96ce2f6a82a))
* Testing additional stages. ([10961d0](https://gitlab.com/zal-tech/test-projects/test-image/commit/10961d069d1c6f76763cda048f91fb296c71eb3d))
* Testing additional stages. ([ccb9ee3](https://gitlab.com/zal-tech/test-projects/test-image/commit/ccb9ee36fd184a03c1c8214101ea788f8306747a))

## [1.0.2](https://gitlab.com/zal-tech/test-projects/test-image/compare/v1.0.1...v1.0.2) (2021-06-12)


### Fix

* Testing release. ([390be56](https://gitlab.com/zal-tech/test-projects/test-image/commit/390be56dd11c9f25c076a714428494c8a06715d8))

## [1.0.1](https://gitlab.com/zal-tech/test-projects/test-image/compare/v1.0.0...v1.0.1) (2021-06-12)


### Fix

* Testing release. ([a19e977](https://gitlab.com/zal-tech/test-projects/test-image/commit/a19e977472c9808135487d4d1e0776aca11bf585))

# 1.0.0 (2021-06-12)


### Fix

* Testing release process. ([fde5a27](https://gitlab.com/zal-tech/test-projects/test-image/commit/fde5a27524091b454489467c620f6e00489e3a98))
* Testing release process. ([d397a57](https://gitlab.com/zal-tech/test-projects/test-image/commit/d397a57cdac4d44011308229c79b78dc525d901d))
* Testing release process. ([f9ed883](https://gitlab.com/zal-tech/test-projects/test-image/commit/f9ed883918c51fa3ee916844686e65d95dec7ac7))
* Testing release process. ([6033537](https://gitlab.com/zal-tech/test-projects/test-image/commit/60335375a1246707a21401b7adc6830650294cdf))
* Updated cicd image version. ([13e6e23](https://gitlab.com/zal-tech/test-projects/test-image/commit/13e6e23e367957fbee4295a918e71989647a6764))
