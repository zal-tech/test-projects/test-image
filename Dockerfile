FROM ubuntu:21.04
SHELL ["/bin/bash", "-euo", "pipefail", "-c"]

# Add shell tools
RUN apt-get update \
    && DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
    git=1:2.30.2-1ubuntu1 \
    vim=2:8.2.2434-1ubuntu1.1 \
    curl=7.74.0-1ubuntu2.3 \
    make=4.3-4ubuntu1 \
    apt-transport-https=2.2.3 \
    ca-certificates=20210119build1 \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# Add developer user and docker group
RUN useradd -m dev \
    && mkdir /home/dev/projects \
    && addgroup docker \
    && usermod -aG docker dev

# Add Docker
RUN curl -L -o docker.deb https://download.docker.com/linux/ubuntu/dists/hirsute/pool/stable/amd64/docker-ce-cli_20.10.6~3-0~ubuntu-hirsute_amd64.deb \
    && dpkg -i docker.deb \
    && rm docker.deb

# Add Hadolint
RUN curl -LO https://github.com/hadolint/hadolint/releases/download/v2.4.0/hadolint-Linux-x86_64 \
    && chmod +x hadolint-Linux-x86_64 \
    && mv hadolint-Linux-x86_64 /usr/local/bin/hadolint

# Add container-structure-test
RUN curl -LO https://storage.googleapis.com/container-structure-test/latest/container-structure-test-linux-amd64 \
    && chmod +x container-structure-test-linux-amd64 \
    && mv container-structure-test-linux-amd64 /usr/local/bin/container-structure-test

# Add Syft and Grype
RUN curl -L -o syft.tar.gz https://github.com/anchore/syft/releases/download/v0.19.1/syft_0.19.1_linux_amd64.tar.gz \
    && mkdir syft \
    && tar -xf syft.tar.gz -C ./syft \
    && mv ./syft/syft /usr/local/bin/syft \
    && rm -r ./syft syft.tar.gz

RUN curl -L -o grype.tar.gz https://github.com/anchore/grype/releases/download/v0.13.0/grype_0.13.0_linux_amd64.tar.gz \
   && mkdir grype \
   && tar -xf grype.tar.gz -C ./grype \
   && mv ./grype/grype /usr/local/bin/grype \
   && rm -r ./grype grype.tar.gz

# Copy over start-dev script
COPY scripts/start-dev.sh /home

# Switch to dev user and handle non-root installations
USER dev

# Add NodeJs (for semantic-release and other scripts/tools)
# Add NVM
RUN curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.38.0/install.sh | bash \
    && source ~/.nvm/nvm.sh \
    && nvm install 16.4.1 \
    && nvm install 14.17.2

# Add Snyk
RUN source ~/.nvm/nvm.sh \
    && npm i -g snyk@1.606.0 \
    snyk-to-html@1.15.0

# Add Semantic Release
RUN source ~/.nvm/nvm.sh \
    && npm i -g semantic-release@17.4.3 \
    @semantic-release/changelog@5.0.1 \
    @semantic-release/commit-analyzer@8.0.1 \
    @semantic-release/exec@5.0.0 \
    @semantic-release/git@9.0.0 \
    @semantic-release/gitlab@6.0.9 \
    @semantic-release/npm@7.1.0 \
    @semantic-release/release-notes-generator@9.0.2 \
    conventional-changelog-eslint@3.0.9

WORKDIR /home
