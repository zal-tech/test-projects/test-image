include:
  - template: Security/Secret-Detection.gitlab-ci.yml
  - template: Security/SAST.gitlab-ci.yml

variables:
  # Temporary dev image for build/testing before release
  DEV_TAG: ci-$CI_COMMIT_SHA

  # Image references
  DIND_IMAGE: docker:20.10.7-dind
  DOCKER_IMAGE: registry.gitlab.com/zal-tech/test-projects/test-image/docker-image-test:0.2.0
  SEMANTIC_RELEASE_IMAGE: registry.gitlab.com/zal-tech/test-projects/test-image/node/sm-image-test:0.1.0
  SNYK_IMAGE: registry.gitlab.com/zal-tech/test-projects/test-image/node/snyk-image-test:0.1.1

  # Docker-in-docker Vars
  DOCKER_HOST: tcp://docker:2376
  DOCKER_TLS_CERTDIR: "/certs"
  DOCKER_TLS_VERIFY: 1
  DOCKER_CERT_PATH: "$DOCKER_TLS_CERTDIR/client"

workflow:
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    - if: $CI_PIPELINE_SOURCE == "web"
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

stages:
  - build
  - test
  - git-release
  - docker-release
  - post-release

# Macros
.docker-cache:
  cache: &docker-cache
    key: ${CI_PIPELINE_ID}
    paths:
      - dev-image.tar

.docker-auth: &docker-auth
  - echo "$CI_JOB_TOKEN" | docker login -u gitlab-ci-token --password-stdin $CI_REGISTRY

.snyk-auth: &snyk-auth
  - snyk auth ${SNYK_TOKEN}

docker-build:
  stage: build
  image: $DOCKER_IMAGE
  services:
    - $DIND_IMAGE
  cache: *docker-cache
  before_script:
    - *docker-auth
  script:
    - docker pull $CI_REGISTRY_IMAGE:latest || true
    - docker build --cache-from $CI_REGISTRY_IMAGE:latest --tag $DEV_TAG .
    - docker save --output dev-image.tar $DEV_TAG

# ToDo: Find a means to utilize this output without requiring paid gitlab.
.anchore:
  stage: test
  image: $DOCKER_IMAGE
  services:
    - $DIND_IMAGE
  cache: *docker-cache
  script:
    - syft docker-archive:dev-image.tar -o json > image-sbom.json
    - grype sbom:image-sbom.json -o json > vulnerabilities.json
  artifacts:
    when: always
    paths:
      - image-sbom.json
      - vulnerabilities.json

hadolint:
  stage: test
  image: $DOCKER_IMAGE
  script:
    - hadolint Dockerfile

container-test:
  stage: test
  image: $DOCKER_IMAGE
  services:
    - $DIND_IMAGE
  cache: *docker-cache
  before_script:
    - *docker-auth
  script:
    - docker load --input dev-image.tar
    - container-structure-test test --image $DEV_TAG --config test/config.yaml -o junit --test-report container-test.xml
  artifacts:
    when: always
    paths:
      - container-test.xml
    reports:
      junit: container-test.xml

snyk-test:
  stage: test
  image: $SNYK_IMAGE
  services:
    - $DIND_IMAGE
  cache: *docker-cache
  before_script:
    - *docker-auth
    - *snyk-auth
  script:
    - docker load --input dev-image.tar
    - snyk container test $DEV_TAG --fail-on=patchable --file=Dockerfile --exclude-base-image-vulns --json | snyk-to-html -o snyk_results.html
  artifacts:
    when: always
    paths:
      - snyk_results.html

semantic-release:
  stage: git-release
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      changes:
        - Dockerfile
        - includes/*
  image: registry.gitlab.com/zal-tech/test-projects/test-image/node/sm-image-test:0.1.1
  variables:
    GITLAB_TOKEN: $RELEASE_TOKEN
  script:
    - semantic-release
    - echo IMAGE_VERSION=$(cat version.txt) > build.env
  artifacts:
    when: always
    reports:
      dotenv: build.env
  resource_group: release

tag-image:
  stage: docker-release
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      changes:
        - Dockerfile
        - includes/*
  image: $DOCKER_IMAGE
  services:
    - $DIND_IMAGE
  cache: *docker-cache
  before_script:
    - *docker-auth
  script:
    - docker load --input dev-image.tar
    - docker tag $DEV_TAG $CI_REGISTRY_IMAGE:$IMAGE_VERSION
    - docker tag $DEV_TAG $CI_REGISTRY_IMAGE:latest
    - docker push -a $CI_REGISTRY_IMAGE

snyk-monitor:
  stage: post-release
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  image: $SNYK_IMAGE
  services:
    - $DIND_IMAGE
  before_script:
    - *docker-auth
    - *snyk-auth
  script:
    - snyk container monitor $CI_REGISTRY_IMAGE:$IMAGE_VERSION --file=Dockerfile --exclude-base-image-vulns
